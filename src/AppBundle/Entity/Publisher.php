<?php

/*
 * This file is part of the Bookean package.
 *
 * (c) Pavel Gushchin <pavel_gushchin@mail.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="publisher")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PublisherRepository")
 *
 * @author Pavel Gushchin <pavel_gushchin@mail.ru>
 */
class Publisher
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $publisher;

    /**
     * @ORM\Column(type="integer")
     */
    private $numberOfBooks = 0;

    /**
     * @ORM\OneToMany(targetEntity="Book", mappedBy="publisher")
     */
    private $books;


    /**
     * Publisher constructor.
     */
    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set publisher
     *
     * @param string $publisher
     *
     * @return Publisher
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * Get publisher
     *
     * @return string
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set numberOfBooks
     *
     * @param integer $numberOfBooks
     *
     * @return Publisher
     */
    public function setNumberOfBooks($numberOfBooks)
    {
        $this->numberOfBooks = $numberOfBooks;

        return $this;
    }

    /**
     * Get numberOfBooks
     *
     * @return integer
     */
    public function getNumberOfBooks()
    {
        return $this->numberOfBooks;
    }

    /**
     * Add book
     *
     * @param \AppBundle\Entity\Book $book
     *
     * @return Publisher
     */
    public function addBook(\AppBundle\Entity\Book $book)
    {
        $this->books[] = $book;

        return $this;
    }

    /**
     * Remove book
     *
     * @param \AppBundle\Entity\Book $book
     */
    public function removeBook(\AppBundle\Entity\Book $book)
    {
        $this->books->removeElement($book);
    }

    /**
     * Get books
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBooks()
    {
        return $this->books;
    }
}
