<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Publisher;

/**
 * Class LoadPublisherData
 *
 * @author Pavel Gushchin <pavel_gushchin@mail.ru>
 */
class LoadPublisherData extends AbstractFixture implements OrderedFixtureInterface
{
    const TOTAL_AMOUNT = 20;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < self::TOTAL_AMOUNT; $i++) {
            $publisher = new Publisher();
            $publisher->setPublisher($faker->company);

            $manager->persist($publisher);
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2;
    }
}
