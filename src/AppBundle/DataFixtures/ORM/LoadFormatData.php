<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Format;

/**
 * Class LoadFormatData
 *
 * @author Pavel Gushchin <pavel_gushchin@mail.ru>
 */
class LoadFormatData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $small = new Format();
        $small->setFormat("120x165");
        $manager->persist($small);

        $medium = new Format();
        $medium->setFormat("130x200");
        $manager->persist($medium);

        $large = new Format();
        $large->setFormat("220x290");
        $manager->persist($large);

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 5;
    }
}
