<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Section;

/**
 * Class LoadSectionData
 *
 * @author Pavel Gushchin <pavel_gushchin@mail.ru>
 */
class LoadSectionData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $artsAndPhotography = new Section();
        $artsAndPhotography->setSection("Arts & Photography");
        $manager->persist($artsAndPhotography);

        $literatureAndFiction = new Section();
        $literatureAndFiction->setSection("Literature & Fiction");
        $manager->persist($literatureAndFiction);

        $history = new Section();
        $history->setSection("History");
        $manager->persist($history);

        $sciFiAndFantasy = new Section();
        $sciFiAndFantasy->setSection("Sci-Fi & Fantasy");
        $manager->persist($sciFiAndFantasy);

        $manager->flush();

        $this->addReference("art", $artsAndPhotography);
        $this->addReference("fiction", $literatureAndFiction);
        $this->addReference("history", $history);
        $this->addReference("sci-fi", $sciFiAndFantasy);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 3;
    }
}
