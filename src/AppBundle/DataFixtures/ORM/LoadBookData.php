<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Book;

/**
 * Class LoadBookData
 *
 * @author Pavel Gushchin <pavel_gushchin@mail.ru>
 */
class LoadBookData extends AbstractFixture implements OrderedFixtureInterface
{
    const TOTAL_AMOUNT = 1000;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $authors = $manager->getRepository('AppBundle:Author')->findAll();
        $publishers = $manager->getRepository('AppBundle:Publisher')->findAll();
        $formats = $manager->getRepository('AppBundle:Format')->findAll();
        $categories = $manager->getRepository('AppBundle:Category')->findAll();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < self::TOTAL_AMOUNT; $i++) {
            $titleWords = $faker->numberBetween(1, 5);
            $annotationParagraphs = $faker->numberBetween(1, 3);

            $book = new Book();
            $book->setTitle($faker->words($titleWords, true));
            $book->setYear($faker->year);
            $book->setPrice($faker->biasedNumberBetween(100, 2000, 'self::linearLow'));
            $book->setPages($faker->numberBetween(50, 800));
            $book->setAnnotation($faker->paragraphs($annotationParagraphs, true));
            $book->setIsbn($faker->numerify('#-###-#####-###-#'));
            $book->setDiscount($faker->randomElement(array(0, 0, 0, 5, 10)));
            $book->setRating(0);
            $book->setWeight($faker->numberBetween(50, 800) / 1000);
            $book->setCirculation($faker->randomElement(array(500, 1000, 2000, 4000, 5000, 10000)));
            $book->setEdition($faker->biasedNumberBetween(1, 5, 'self::linearLow'));
            $book->setNumberOfComments(0);
            $book->setInStock($faker->biasedNumberBetween(0, 1000, 'self::linearHigh'));
            $book->setOrderedNumberOfTimes($faker->numberBetween(0, 1000));
            $book->setCreatedAt($faker->dateTime);

            $randomAuthor = $faker->randomElement($authors);
            $book->addAuthor($randomAuthor);

            $randomPublisher = $faker->randomElement($publishers);
            $book->setPublisher($randomPublisher);

            $randomFormat = $faker->randomElement($formats);
            $book->setFormat($randomFormat);

            $randomCategory = $faker->randomElement($categories);
            $book->setCategory($randomCategory);

            $manager->persist($book);
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 6;
    }
}
