<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Category;

/**
 * Class LoadCategoryData
 *
 * @author Pavel Gushchin <pavel_gushchin@mail.ru>
 */
class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $architecture = new Category();
        $architecture->setCategory("Architecture");
        $architecture->setSection($this->getReference("art"));
        $manager->persist($architecture);

        $design = new Category();
        $design->setCategory("Design");
        $design->setSection($this->getReference("art"));
        $manager->persist($design);

        $painting = new Category();
        $painting->setCategory("Painting");
        $painting->setSection($this->getReference("art"));
        $manager->persist($painting);


        $actionAndAdventure = new Category();
        $actionAndAdventure->setCategory("Action & Adventure");
        $actionAndAdventure->setSection($this->getReference("fiction"));
        $manager->persist($actionAndAdventure);

        $classics = new Category();
        $classics->setCategory("Classics");
        $classics->setSection($this->getReference("fiction"));
        $manager->persist($classics);

        $poetry = new Category();
        $poetry->setCategory("Poetry");
        $poetry->setSection($this->getReference("fiction"));
        $manager->persist($poetry);


        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 4;
    }
}
