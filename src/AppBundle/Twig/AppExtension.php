<?php

/*
 * This file is part of the Bookean package.
 *
 * (c) Pavel Gushchin <pavel_gushchin@mail.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Twig;

use AppBundle\Service\CartManagement\CartInterface;
use Symfony\Component\Intl\Intl;

class AppExtension extends \Twig_Extension
{
    /**
     * @var string
     */
    private $locales;

    /**
     * @var CartInterface
     */
    private $cart;

    /**
     * AppExtension constructor.
     *
     * @param string $locales
     * @param CartInterface $cart
     *
     */
    public function __construct($locales, CartInterface $cart)
    {
        $this->locales = $locales;
        $this->cart = $cart;

        /*$token = $this->tokenStorage->getToken();
        $user = $token ? $token->getUser() : null;

        if ($token && $user instanceof User) {
            $this->booksInCart = $user->getCartItems() ?: array();
        } else {
            $this->booksInCart = $this->session->get('booksInCart') ?: array();
        }*/
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('locales', array($this, 'getLocales')),
            new \Twig_SimpleFunction('isBookInCart', array($this, 'isBookInCart')),
            new \Twig_SimpleFunction('amountOfBooksInCart', array($this, 'amountOfBooksInCart')),
            new \Twig_SimpleFunction('totalPrice', array($this, 'getTotalPrice')),
        );
    }

    /**
     * Takes the list of codes of the locales (languages) enabled in the
     * application and returns an array with the name of each locale written
     * in its own language (e.g. English, Русский, etc.)
     *
     * @return array
     */
    public function getLocales()
    {
        $locales = array();

        $localeCodes = explode('|', $this->locales);
        foreach ($localeCodes as $localeCode) {
            $locales[] = array(
                'code' => $localeCode,
                'name' => Intl::getLocaleBundle()
                    ->getLocaleName($localeCode, $localeCode)
            );
        }

        return $locales;
    }


    public function isBookInCart($checkedBookId)
    {
        return $this->cart->isBookInCart($checkedBookId);

        /*$token = $this->tokenStorage->getToken();
        $user = $token ? $token->getUser() : null;

        if ($token && $user instanceof User) {
            return $this->isBookInDbCart($checkedBookId);
        } else {
            return $this->isBookInSessionCart($checkedBookId);
        }*/
    }

    public function amountOfBooksInCart()
    {
        return $this->cart->getAmountOfItems();
    }

    public function getTotalPrice()
    {
        return $this->cart->getTotalPrice();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app.extension';
    }
}