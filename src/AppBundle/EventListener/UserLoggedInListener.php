<?php

namespace AppBundle\EventListener;

use AppBundle\Service\CartManagement\LoggedInUserCart;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class UserLoggedInListener
{
    /**
     * @var LoggedInUserCart
     */
    private $loggedInUserCart;

    /**
     * UserLoggedInListener constructor.
     *
     * @param LoggedInUserCart $loggedInUserCart
     */
    public function __construct(LoggedInUserCart $loggedInUserCart)
    {
        $this->loggedInUserCart = $loggedInUserCart;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $this->loggedInUserCart->synchronize();
    }
}
