<?php

namespace AppBundle\Service\CartManagement;

use AppBundle\Entity\Book;
use AppBundle\Entity\CartItem;


interface CartInterface
{
    /**
     * @return int
     */
    public function getAmountOfItems();

    /**
     * @return CartItem[]
     */
    public function getAllItems();

    /**
     * @param int $bookId
     *
     * @return bool
     */
    public function isBookInCart($bookId);

    /**
     * @param int $bookId
     */
    public function addBookToCart($bookId);

    /**
     * Remove all items from cart
     */
    public function removeAll();

    /**
     * @param int $bookId
     */
    public function remove($bookId);

    /**
     * @param int $bookId
     * @param int $amount
     */
    public function setAmount($bookId, $amount);

    /**
     * @return float
     */
    public function getTotalPrice();
}
