<?php

namespace AppBundle\Service\CartManagement;

use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CartFactory
{
    /**
     * @param ObjectManager $entityManager
     * @param Session $session
     * @param TokenStorage $tokenStorage
     *
     * @return CartInterface
     */
    public function createCart(ObjectManager $entityManager, Session $session, TokenStorage $tokenStorage)
    {
        $userLoggedIn = $this->getUser($tokenStorage);

        if ($userLoggedIn) {
            return new LoggedInUserCart($userLoggedIn, $entityManager, $session);
        } else {
            return new AnonUserCart($entityManager, $session);
        }
    }

    /**
     * @internal Get a user from the Security Token Storage.
     *
     * @param TokenStorage $tokenStorage
     *
     * @return User|null
     */
    private function getUser(TokenStorage $tokenStorage)
    {
        $token = $tokenStorage->getToken();

        if (null === $token) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            return null;
        }

        return $user;
    }
}
