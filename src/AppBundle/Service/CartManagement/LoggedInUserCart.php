<?php

namespace AppBundle\Service\CartManagement;

use AppBundle\Entity\CartItem;
use AppBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class LoggedInUserCart implements CartInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var CartItem[]
     */
    private $itemsInCart;

    /**
     * @var float
     */
    private $totalPrice = 0;

    /**
     * CartManager constructor.
     *
     * @param ObjectManager $entityManager
     * @param User $user
     */
    public function __construct(User $user, ObjectManager $entityManager, Session $session)
    {
        $this->user = $user;
        $this->entityManager = $entityManager;
        $this->session = $session;

        $this->getAllCartItemsFromDatabase();
        $this->calculateTotalPriceForAllItems();
    }

    /**
     * {@inheritdoc}
     */
    public function getAllItems()
    {
        return $this->itemsInCart;
    }

    /**
     * {@inheritdoc}
     */
    public function getAmountOfItems()
    {
        $totalAmount = 0;

        foreach ($this->itemsInCart as $item) {
            $totalAmount += $item->getAmount();
        }

        return $totalAmount;
    }

    /**
     * {@inheritdoc}
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * {@inheritdoc}
     */
    public function isBookInCart($bookId)
    {
        foreach ($this->itemsInCart as $item) {
            if ($bookId == $item->getBook()->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function addBookToCart($bookId)
    {
        if ($this->isBookInCart($bookId)) {
            return;
        }

        $book = $this->entityManager->getRepository('AppBundle:Book')->find($bookId);
        if (!$book) {
            return;
        }

        $cartItem = new CartItem();
        $cartItem->setUser($this->user);
        $cartItem->setBook($book);
        $cartItem->setAmount(1);
        $cartItem->setCreatedAt(new \DateTime());

        $this->entityManager->persist($cartItem);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function removeAll()
    {
        $this->entityManager->getRepository('AppBundle:CartItem')->removeAll($this->user);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($bookId)
    {
        if (!$this->isBookInCart($bookId)) {
            return;
        }

        $book = $this->entityManager->getRepository('AppBundle:Book')->find($bookId);
        if (!$book) {
            return;
        }

        $this->entityManager->getRepository('AppBundle:CartItem')->remove($this->user, $book);
    }

    /**
     * {@inheritdoc}
     */
    public function setAmount($bookId, $amount)
    {
        if (!$this->isBookInCart($bookId)) {
            return;
        }

        $book = $this->entityManager->getRepository('AppBundle:Book')->find($bookId);
        if (!$book) {
            return;
        }

        $this->entityManager->getRepository('AppBundle:CartItem')->setAmount($this->user, $book, $amount);
    }


    public function synchronize()
    {
        $anonUserCart = new AnonUserCart($this->entityManager, $this->session);

        if (0 === $anonUserCart->getAmountOfItems()) {
            return;
        }

        foreach ($anonUserCart->getAllItems() as $item) {
            $checkedBookId = $item['book']['id'];

            if ($this->isBookInCart($checkedBookId)) {
                continue;
            }

            $checkedBook = $this->entityManager->getRepository('AppBundle:Book')->find($checkedBookId);

            if (!$checkedBook) {
                continue;
            }

            $cartItem = new CartItem();
            $cartItem->setBook($checkedBook);
            $cartItem->setUser($this->user);
            $cartItem->setAmount(1);
            $cartItem->setCreatedAt(new \DateTime());

            $this->entityManager->persist($cartItem);
        }

        $this->entityManager->flush();

        $this->getAllCartItemsFromDatabase();
        $this->calculateTotalPriceForAllItems();
    }

    /**
     * @internal
     */
    private function getAllCartItemsFromDatabase()
    {
        $this->itemsInCart = $this->entityManager->getRepository('AppBundle:CartItem')->findAllItemsByUser($this->user) ?: array();
    }

    /**
     * @internal
     */
    private function calculateTotalPriceForAllItems()
    {
        foreach ($this->itemsInCart as $item) {
            $this->totalPrice += $this->calcPriceWithDiscount($item);
        }
    }

    /**
     * @internal
     *
     * @param CartItem $item
     *
     * @return float
     */
    private function calcPriceWithDiscount(CartItem $item)
    {
        $fullPrice = $item->getBook()->getPrice();
        $discount = $item->getBook()->getDiscount();

        return $fullPrice * (1 - $discount/100);
    }
}
