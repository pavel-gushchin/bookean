<?php

namespace AppBundle\Service\CartManagement;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class AnonUserCart implements CartInterface
{
    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var array
     */
    private $itemsInCart;

    /**
     * @var float
     */
    private $totalPrice = 0;

    /**
     * CartManager constructor.
     *
     * @param ObjectManager $entityManager
     * @param Session $session
     * @param TokenStorage $tokenStorage
     */
    public function __construct(ObjectManager $entityManager, Session $session)
    {
        $this->entityManager = $entityManager;
        $this->session = $session;

        $this->itemsInCart = $this->session->get('itemsInCart') ?: array();

        foreach ($this->itemsInCart as $item) {
            $this->totalPrice += $this->calcPriceWithDiscount($item);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAllItems()
    {
        return $this->itemsInCart;
    }

    /**
     * {@inheritdoc}
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * {@inheritdoc}
     */
    public function getAmountOfItems()
    {
        $totalAmount = 0;

        foreach ($this->itemsInCart as $item) {
            $totalAmount += $item['amount'];
        }

        return $totalAmount;
    }

    /**
     * {@inheritdoc}
     */
    public function isBookInCart($bookId)
    {
        foreach ($this->itemsInCart as $item) {
            if ($bookId == $item['book']['id']) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function addBookToCart($bookId)
    {
        if ($this->isBookInCart($bookId)) {
            return;
        }

        $book = $this->entityManager->getRepository('AppBundle:Book')->find($bookId);
        if (!$book) {
            return;
        }

        $this->itemsInCart[] = array(
            'book' => array(
                'id' => $book->getId(),
                'title' => $book->getTitle(),
                'price' => $book->getPrice(),
                'discount' => $book->getDiscount()
            ),
            'amount' => 1
        );

        $this->session->set('itemsInCart', $this->itemsInCart);
    }

    public function removeAll()
    {
        $this->session->set('itemsInCart', array());
    }

    public function remove($bookId)
    {
        foreach ($this->itemsInCart as &$item) {
            if ($bookId == $item['book']['id']) {
                unset($item);
                break;
            }
        }
        unset($item);

        $this->session->set('itemsInCart', $this->itemsInCart);
    }

    public function setAmount($bookId, $amount)
    {
        foreach ($this->itemsInCart as &$item) {
            if ($bookId == $item['book']['id']) {
                $item['amount'] = $amount;
                break;
            }
        }
        unset($item);

        $this->session->set('itemsInCart', $this->itemsInCart);
    }

    /**
     * @internal
     *
     * @param array $item
     *
     * @return float
     */
    private function calcPriceWithDiscount(array $item)
    {
        $fullPrice = $item['book']['price'];
        $discount = $item['book']['discount'];

        return $fullPrice * (1 - $discount/100);
    }
}
