<?php

/*
 * This file is part of the Bookean package.
 *
 * (c) Pavel Gushchin <pavel_gushchin@mail.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Book;
use AppBundle\Entity\CartItem;
use AppBundle\Entity\Review;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 *
 * @author Pavel Gushchin <pavel_gushchin@mail.ru>
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     * @Cache(smaxage="10")
     */
    public function indexAction()
    {
        /**
         * @var \Doctrine\ORM\EntityManager $em
         */
        $em = $this->getDoctrine()->getManager();

        $newestBooks = $em->getRepository('AppBundle:Book')->findNewest();
        $bestsellers = $em->getRepository('AppBundle:Book')->findBestsellers();

        return $this->render('default/index.html.twig', array(
            'newestBooks' => $newestBooks,
            'bestsellers' => $bestsellers
        ));
    }

    /**
     * @Route("/book/{id}", name="book_show", requirements={"id": "[1-9]\d*"})
     * @Method("GET")
     */
    public function showBookAction(Book $book)
    {
//        $book = $this->getDoctrine()->getRepository('AppBundle:Book')->find($id);

        return $this->render('default/book_show.html.twig', array(
            'book' => $book
        ));
    }

    /**
     * @Route("/cart/add/{bookId}/amount/{amount}",
     *     name = "add_book_to_cart",
     *     requirements = {
     *         "bookId": "[1-9]\d*",
     *         "amount": "[1-9]\d*"
     *     }
     * )
     * @Method("GET")
     */
    public function addBookToCartAction($bookId, $amount)
    {
        $this->get('app.cart')->addBookToCart($bookId);

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/comment/{bookId}/new", name="review_new")
     * @Method("POST")
     * @Security("is_granted('ROLE_USER')")
     * @ParamConverter("book", options={"mapping": {"bookId": "id"}})
     */
    public function reviewNewAction(Request $request, Book $book)
    {
        $form = $this->createForm('AppBundle\Form\ReviewType');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Review $review */
            $review = $form->getData();
            $review->setAuthorName($this->getUser()->getUsername());
            $review->setBook($book);
            $review->setUser($this->getUser());
            $review->setCreatedAt(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($review);
            $entityManager->flush();

            return $this->redirectToRoute('book_show', array('id' => $book->getId()));
        }

        return $this->render('default/review_form_error.html.twig', array(
            'book' => $book,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Book $book
     *
     * @return Response
     */
    public function reviewFormAction(Book $book)
    {
        $form = $this->createForm('AppBundle\Form\ReviewType');

        return $this->render('default/_review_form.html.twig', array(
            'book' => $book,
            'form' => $form->createView(),
        ));
    }
}
