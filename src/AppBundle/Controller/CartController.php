<?php

/*
 * This file is part of the Bookean package.
 *
 * (c) Pavel Gushchin <pavel_gushchin@mail.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 *
 *
 * @author Pavel Gushchin <pavel_gushchin@mail.ru>
 */
class CartController extends Controller
{

    /**
     * @Route("/cart", name="cart")
     * @Method("GET")
     */
    public function showBooksInCartAction()
    {
        $cart = $this->get('app.cart');

        return $this->render('cart/cart.html.twig', array(
            'cartItems' => $cart->getAllItems(),
//            'totalAmount' => $cart->getAmountOfItems(),
//            'totalPrice' => $cart->getTotalPrice()
        ));
    }
}
